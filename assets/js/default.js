$(document).ready(function() {

    "use strict";

    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var date = (day + "/" + month + "/" + year);
    var page = document.getElementsByName('page');
    var page_name = $(page).val();
    var course_id = $("#course_id").val();
    var searchText = $('#search');
    var docWidth = $(document).width();
    var docHeight = $(document).outerHeight();
    var target;


    function initTabs() {
        if (docWidth > 480) {
            $('.tabs, .blog-tabs').tabs({fx: {opacity: 'toggle'}});
        }
    }

    $('.selectBox').selectBox();
    $('.socialf a[title]').tooltip({placement: 'bottom'});
    $('.tooltip').tooltip();
    $('#checkout input').customInput();
    $('#reply-attachment').customFileInput();
    $('.carousel').carousel({interval: 8000});
    $('#search_form .search').typeahead({
        source: function(query, process) {
            return $.getJSON(base_url + 'search.php',
                    {query: query},
            function(data) {
                return process(data);
            });
        }

    });
    initTabs();
    $(window).resize(function(){
        docWidth = $(document).outerWidth();
        docHeight = $(document).outerHeight();
        initTabs();
    });

    $('.ty-overlay').css({'height': $(document).height(), 'width': $(document).width()});
    $('.ty-popup form').submit(function(e) {

        e.preventDefault();

        if ($('#user_source').val() !== '') {
            $.post(base_url + 'cart/update_order', {email: $('#email').val(), user_source: $('#user_source').val()})

                    .done(function(data, status) {
                $('.ty-popup').fadeOut('slow');
                $('.ty-overlay').fadeOut('slow');
            });
        } else {
            alert("Please select How did you hear about us?");
        }

    });

    $('#catNav li a.head').click(function() {
        if ($(this).hasClass('active')) {
            $target = $(this).parent().children('ul.sub');
            if ($target.length) {
                $target.slideUp();
                $(this).removeClass('active');
            }
        }
        else {
            $target = $(this).parent().children('ul.sub');
            if ($target.length) {
                $target.slideDown();
                $(this).addClass('active');
            }
        }
        return false;
    });

    $('#header .navbar button.btn-navbar').click(function(e) {
        $(this).toggleClass('active');
        if (!$(this).hasClass('hasBeenClicked')) {
            $.ajax({
                url: base_url + 'headerAjax.php?action=mobileAjax',
                type: 'post',
                success: function(html) {
                    $('#mobileSubNavWrap .mainCat').empty().append(html);//html(html);
                },
                complete: function() {

                    $('.mobileSubNav').slideToggle('slow');
                    $('#mobileSubNavWrap ul.mainCat > li > a').click(function(e) {
                        if (!$(this).hasClass('active')) {
                            $('#mobileSubNavWrap ul.mainCat > li > a.active').next().slideUp('slow');
                            $('#mobileSubNavWrap ul.mainCat > li > a.active').removeClass('active');
                        }
                        $(this).toggleClass('active');
                        target = $(this).next();
                        $(target).slideToggle('slow');
                        e.preventDefault();
                    });

                    $('#mobileSubNavWrap ul.subCat > li > a').click(function(e) {
                        if (!$(this).hasClass('active')) {
                            $('#mobileSubNavWrap ul.subCat > li > a.active').next().slideUp('slow');
                            $('#mobileSubNavWrap ul.subCat > li > a.active').removeClass('active');
                        }
                        $(this).toggleClass('active');
                        target = $(this).next();
                        $(target).slideToggle('slow');
                        e.preventDefault();
                    });

                }
            });
            $(this).addClass('hasBeenClicked');
        }
        else {
            $('.mobileSubNav').slideToggle('slow');
        }
        e.preventDefault();
    });

    $('#primary-nav ul > li:not(.basketInfo) > a').click(function(e) {
        var menuLink = $(this);
        if (!$('#primary-nav').hasClass('hasBeenClickedDesktop')) {
            $.ajax({
                url: base_url + 'headerAjax.php?action=desktopAjax',
                type: 'post',
                dataType: 'html',
                success: function(html, status) {
                    $('#subNav-wrap').empty().html(html);
                },
                complete: function() {

                    $('.course_nav_list li:first-child a').addClass('active');

                    target = $(menuLink).attr('id');
                    target = $('#' + target + 'topnav');
                    target.slideToggle('slow').toggleClass('active');

                    $('.course_nav_list li a').click(function(e) {
                        if (!$(this).hasClass('active')) {
                            $(this).closest('.course_nav_list').find('li a').removeClass('active');
                            $(this).addClass('active');
                            target = $(this).attr('id');
                            target = $('#' + target + 'Nav');
                            target.parent().find('.activeNav').removeClass('activeNav');
                            target.addClass('activeNav');
                        }
                        e.preventDefault();
                    });

                    $('.sprite-navArrow').click(function(e) {
                        e.preventDefault();
                        $('.subNav').slideUp('slow', function() {
                            $('.subNav').removeClass('active');
                            $('#primary-nav ul > li > a').removeClass('selectedNav');
                        });
                    });

                }
            });
            $('#primary-nav').addClass('hasBeenClickedDesktop');
        }
        if (!menuLink.hasClass('selectedNav')) {
            $('.subNav.active').slideUp('slow');
            $('.subNav.active').removeClass('active');
            $('#primary-nav ul > li > a').removeClass('selectedNav');
        }
        $(menuLink).toggleClass('selectedNav');
        target = $(menuLink).attr('id');
        target = $('#' + target + 'topnav');
        target.slideToggle('slow').toggleClass('active');
        //e.preventDefault();
        return false;
    });

    $('.course_nav_list li a').dblclick(function(e) {
        e.preventDefault();
    });

    $('#checkout div.colB input[type=radio]').click(function() {
        if ($(this).hasClass('creditCard')) {
            $('#creditCardOption').slideDown();
        }
        else {
            $('#creditCardOption').slideUp();
        }
    });

    searchText.change(function() {
        $('#search_form').attr('action', base_url + 'search/' + searchText.val());
    });

    $('#courses_list_description a.read_more_js').toggle(function() {

        var currentHeight = $("#courses_list_text").css("height");
        $("#courses_list_text").css("height", "auto");
        var animateHeight = $("#courses_list_text").css("height");
        $("#courses_list_text").css("height", currentHeight);
        $('a.read_more_js').html('Read Less');
        $('#courses_list_text').animate({
            height: animateHeight
        }, 1500);
    }, function() {
        $('a.read_more_js').html('Read More');
        $('#courses_list_text').animate({
            height: 50
        }, 1500);
    });

    initDropdown();
    applyPagination();
    
    function applyPagination() {
      $("#ajax_paging li:not(.currentPage) a").click(function(e) {
        var url = $(this).attr("href");
        console.log(url);
        $.ajax({
          url: url,
          success: function(msg) {
            $("#review_content").html($(msg).find('#review_content').html());
            $('html, body').animate({
                scrollTop: $("#review_content").offset().top
            }, 2000);
            applyPagination();
            initDropdown();
          }
        });
        e.preventDefault();
      });
    }

    function initDropdown() {
      $('.review-rm').click(function(e) {
        var id = $(this).attr('id');
        if ($('#review-' + id).hasClass('active')) {
            $('#review-' + id).toggleClass('active', 1000);
            $(this).html('Read more');
        }
        else {
            $('#review-' + id).toggleClass('active', 1000);
            $(this).html('Read less');
        }
        e.preventDefault();
    });
    }
    
    $('#course-branch > a').click(function(e) {
        e.preventDefault();
        $('#course-branch').toggleClass('active');
    });

    //data tables
    $('#personal').dataTable({
        "aaSorting": [],
        "aLengthMenu": [[25, 50, -1], [25, 50, "All"]],
        "iDisplayLength": 25,
        "sPaginationType": 'full_numbers',
        "sDom": 'rt<"bottom"ilp><"clear">',
        "oTableTools": {
            "sSwfPath": "/templates/js/libs/datatables/swf/copy_cvs_xls_pdf.swf"
        },
        'oLanguage': {
            'sLengthMenu': '<span class="label">Show per page:</span> _MENU_',
            'sZeroRecords': 'Nothing found - sorry',
            'sInfo': '_START_ to _END_ of _TOTAL_ courses',
            'sInfoEmpty': '0 to 0 of 0 records',
            'sInfoFiltered': '(filtered from _MAX_ total courses)'
        }


    });
    if (page_name === "bookings") {
        $('<input type="submit" class="buttonA aleft" style="width:90px; height:22px; padding:0; margin:10px 0 0 5px; font-size:12px;" value="Message"><input type="button"  onclick="certificates();" class="buttonA aleft" style="width:90px; height:22px; padding:0; margin:10px 0 0 5px; font-size:12px;" value="Print">').insertAfter('#personal');
    }
    if (page_name === "trainer_files") {
        $('<input type="submit" class="buttonA aleft" style="width:90px; height:22px; padding:0; margin:10px 0 0 5px; font-size:12px;" value="Delete">').insertAfter('#personal');
    }


    $('#datea').datepicker({
        dateFormat: "dd M yy"
    });
    $('#dateb').datepicker({
        dateFormat: "dd M yy"
    });

    $("#imageSlider").jCarouselLite({
        btnNext: '#imageSliderNext',
        btnPrev: '#imageSliderPrev',
        scroll: 1,
        visible: 4
    });
    $("a.fancybox").fancybox({
        'zoomSpeedIn': 500,
        'zoomSpeedOut': 500,
        'overlayOpacity': .6,
        'overlayColor': '#000',
        'transitionIn': 'elastic',
        'transitionOut': 'elastic'
    });
    $("a[rel=example_group]").fancybox({
        'transitionIn': 'none',
        'transitionOut': 'none',
        'titlePosition': 'over',
        'titleFormat': function(title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over">Image :' + title + '</span>';
        }
    });


    $("input.masterCheck").click(function() {
        if ($('#checkAll').is(':checked')) {
            $("#personal td.check input").attr('checked', $('#checkAll').is(':checked'));
            $('.custom-checkbox label').addClass('checked');
        }
        else {
            $("#personal td.check input").attr('checked', $('#checkAll').is(':checked'));
            $('.custom-checkbox label').removeClass('checked');
        }

    });

    $("#clientsSlider").jCarouselLite({
        btnNext: '#clientsSliderNext',
        btnPrev: '#clientsSliderPrev',
        scroll: 1,
        visible: 5
    });

    $('#category_id').change(function() {
        $('#rate_form').attr("action", "/ratecourse/detail/").submit();
    });

    function rewrite_form()
    {
        form_action = document.getElementById('rate_form').action;
        value_to_add = document.getElementById('product_id').value;
        document.getElementById('rate_form').action = "/ratecourse/detail/" + value_to_add;
        document.getElementById('rate_form').submit();
    }

    $('#next-btn').click(function(e) {
        var formAction = $('#rate_form').attr('action');
        var value_to_add = $('#product_id').attr('value');
        $('#rate_form').attr('action', '/ratecourse/detail/' + value_to_add);
        $('#rate_form').submit();
        e.preventDefault();
    });

    $('#team #imageSlider ul li:nth-child(5) .member-wrap').addClass('active');
    $('#team #members .member:first').addClass('active');
    var firstListHeight = $('#team #members .member ol li:first-child').height();
    //$('#team .member ol').css('height', firstListHeight);

    $('.member-wrap').click(function() {
        $('#team #imageSlider .active').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('data-id');
        $('#members .member.active').removeClass('active').fadeOut('slow', function() {
            $('#team-m' + id).addClass('active').fadeIn('slow');
        });
    });

    $('#team .more-facts').click(function(e) {
        $(this).parent().prev().parent().toggleClass('active');
        e.preventDefault();
    });

    $('#student_data').click(function(e) {
        if (this.checked) {
            $('#firstName').val($('#fname').val());
            $('#lastName').val($('#lname').val());
            $('#emailAddress').val($('#email').val());
            $('#homePhone').val($('#phone').val());
            $('#mobilePhone').val($('#mobile').val());
            $('#postalCode').val($('#pcode').val());
            $('#address1').val($('#address').val());
            $('#companyName').val($('#company').val());
            $('#bcity').val($('#city').val());
        } else {
            $('#firstName').val('');
            $('#lastName').val('');
            $('#emailAddress').val('');
            $('#homePhone').val('');
            $('#mobilePhone').val('');
            $('#postalCode').val('');
            $('#address1').val('');
            $('#companyName').val('');
            $('#bcity').val('');
        }
    });

    $('#student_detail').click(function() {
        if (this.checked) {
            $('#student_details').fadeIn('slow');
        } else {
            $('#student_details').fadeOut('slow');
        }
        $('#checkoutWrap .colC').toggleClass('studentDetails');
    });

    $('#checkoutWrap [id^="paymentMethod"]').click(function() {
        if (!$(this).hasClass('active')) {
            $('#checkoutWrap input').removeClass('active');
            $(this).addClass('active');
            $('#checkoutWrap .colB div[id*="paymentMethod"]').fadeOut('slow');
            target = "#" + $(this).attr('id') + "_Text";
            $(target).fadeIn('slow');
        }

    });

    if (page_name === "my_messages") {
        $('.bottom').prepend('<input type="submit" class="buttonA aleft" style="width:70px; height:22px; padding:0; margin:10px 0 0 5px; font-size:12px;" value="Close"><input type="button" class="buttonA aleft" style="width:70px; height:22px; padding:0; margin:10px 0 0 5px; font-size:12px;" value="Delete" onclick = delete_m();>');
    }

    if (content == 'course') {
        Modernizr.load(assets_url + 'css/fancybox.css');
    }



//end
});

